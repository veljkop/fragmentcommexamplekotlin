package si.uni_lj.fri.pbd.fragmentcommexamplekotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import si.uni_lj.fri.pbd.fragmentcommexamplekotlin.databinding.ActivityMainBinding

// Activity implements a listener defined in a Fragment
// so that it can get notified when an action is taken in one Fragment
// and then can start a corresponding reaction in another Fragment
// TODO: implement listener interface
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    companion object {
        const val TAG = "MainActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    // TODO: implement interface methods


}