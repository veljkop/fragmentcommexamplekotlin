package si.uni_lj.fri.pbd.fragmentcommexamplekotlin

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import si.uni_lj.fri.pbd.fragmentcommexamplekotlin.databinding.FragmentToolbarBinding

/**
 * A simple [Fragment] subclass.
 * Use the [ToolbarFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ToolbarFragment : Fragment(), SeekBar.OnSeekBarChangeListener {

    private var _binding: FragmentToolbarBinding? = null
    private val binding get() = _binding!!

    // TODO: Add activity callback


    var seekvalue = 10

    // TODO: define a listener interface


    override fun onAttach(context: Context) {
        super.onAttach(context)

        // TODO: reference a listener


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.seekBar1.setOnSeekBarChangeListener(this)
        binding.button1.setOnClickListener{ v: View -> buttonClicked(v)}
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentToolbarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun buttonClicked(view: View) {
        // TODO: call listener

    }

    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
        seekvalue = p1
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {
    }

    override fun onStopTrackingTouch(p0: SeekBar?) {
    }
}